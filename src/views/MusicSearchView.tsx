import * as React from "react";
import { SearchAlbums, AlbumsSearchResults } from "../containers/Search";

export class MusicSearchView extends React.Component {
  render() {
    return (
      <div>
        <div className="row">
          <div className="col">
            <SearchAlbums />
          </div>
        </div>

        <div className="row">
          <div className="col">
            <AlbumsSearchResults />
          </div>
        </div>
      </div>
    );
  }
}
