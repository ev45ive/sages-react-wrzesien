import * as React from "react";
import { PlaylistListContainer } from "src/containers/PlaylistsListContainer";
import { PlaylistDetailsC, PlaylistFormC } from "../containers/Playlist";
import { Route } from "react-router-dom";
import { RouteComponentProps } from "react-router";

type IProps = RouteComponentProps<{}>;

export class PlaylistsView extends React.Component<IProps> {
  render() {
    return (
      <div className="row">
        <div className="col">
          <Route
            path="/playlists"
            exact={true}
            component={PlaylistListContainer}
          />
          <Route path="/playlists/:id" component={PlaylistListContainer} />
        </div>
        <div className="col">
          <Route
            path="/playlists/:id"
            exact={true}
            component={PlaylistDetailsC}
          />
          <Route path="/playlists/:id/edit" component={PlaylistFormC} />
        </div>
      </div>
    );
  }
}
