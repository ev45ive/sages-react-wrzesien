import { connect } from "react-redux";
import { AppState } from "src/store";
import {
  IProps,
  IDispatchProps,
  PlaylistForm
} from "../components/PlaylistForm";
import { Dispatch } from "redux";
import { PlaylistDetails } from "../components/PlaylistDetails";
import { Playlist } from "src/model/Playlist";
import { updatePlaylist } from "src/reducers/playlists";
import { RouteComponentProps } from "react-router";

type IOwnProps = RouteComponentProps<{ id: string }>;

export const withPlaylist = connect<
  IProps,
  IDispatchProps,
  IOwnProps,
  AppState
>(
  (state: AppState, ownProps) => ({
    playlist: state.playlists.playlists.find(
      p => p.id == parseInt(ownProps.match.params.id, 10)
    )!
  }),
  (dispatch: Dispatch) => ({
    onCancel: () => {},
    onSave: (playlist: Playlist) => {
      dispatch(updatePlaylist(playlist));
    }
  })
);

export const PlaylistDetailsC = withPlaylist(PlaylistDetails);
export const PlaylistFormC = withPlaylist(PlaylistForm);
