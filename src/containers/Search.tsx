import { connect } from "react-redux";
import { QueryField } from "../components/QueryField";
import { AlbumsGrid } from "../components/AlbumsGrid";
import { Album } from "src/model/Album";
import { AppState } from "src/store";
import { fetchSearchResults } from "../services/services";

type TStateProps = {
  results: Album[];
  query: string;
};

type TDispatchProps = {
  onSearch(query: string): void;
};

export const withSearch = connect<TStateProps, TDispatchProps, {}, AppState>(
  (state: AppState) => ({
    results: state.search.results,
    query: state.search.query
  }),
  dispatch => ({
    onSearch: fetchSearchResults(dispatch)
  })
);

export const SearchAlbums = withSearch(QueryField);
export const AlbumsSearchResults = withSearch(AlbumsGrid);
