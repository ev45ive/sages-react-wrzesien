import { connect, MapStateToProps, MapDispatchToProps } from "react-redux";
import {
  IProps,
  PlaylistList,
  IDispatchProps
} from "src/components/PlaylistList";
import { AppState } from "../store";
import { Dispatch } from "redux";
import { Playlist } from "src/model/Playlist";
import { selectPlaylist } from "src/reducers/playlists";
import { withRouter, RouteComponentProps } from "react-router-dom";

type IOwnProps = RouteComponentProps<{ id: string }>;

const mapStateToProps: MapStateToProps<IProps, IOwnProps, AppState> = (
  state,
  ownProps
) => {
  return {
    playlists: state.playlists.playlists,
    selectedId: parseInt(ownProps.match.params.id, 10)
  };
};

const mapDispatchToProps: MapDispatchToProps<IDispatchProps, IOwnProps> = (
  dispatch: Dispatch,
  ownProps
) => ({
  onSelect: (id: Playlist["id"]) => {
    ownProps.history.push({
      pathname: "/playlists/" + id
    });
    dispatch(selectPlaylist(id));
  }
});

export const PlaylistListContainer = withRouter(
  connect<IProps, IDispatchProps, IOwnProps, AppState>(
    mapStateToProps,
    mapDispatchToProps
  )(PlaylistList)
);
