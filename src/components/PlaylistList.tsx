import * as React from "react";
import { Playlist } from "src/model/Playlist";

export interface IProps {
  playlists: Playlist[];
  selectedId?: Playlist["id"];
}
export interface IDispatchProps {
  onSelect(selectedId: Playlist["id"]): void;
}

export class PlaylistList extends React.PureComponent<IProps & IDispatchProps> {

  render() {
    return (
      <div>
        <div className="list-group">
          {this.props.playlists.map((playlist, index) => (
            <div
              className={`list-group-item ${
                playlist.id == this.props.selectedId ? "active" : ""
              }`}
              key={playlist.id}
              // Bo tak mi pasowało ;-)
              // tslint:disable-next-line:jsx-no-lambda
              onClick={() => this.props.onSelect(playlist.id)}
            >
              {index + 1}. {playlist.name}
            </div>
          ))}
        </div>
      </div>
    );
  }
}

// onSelect(event: React.MouseEvent<HTMLDivElement>) {
//   console.log(event.target.dataset.id);
// }
//data-id={playlist}

// onClick={() => this.props.onSelect(playlist.id)}
// onClick={this.props.onSelect.bind(null,[playlist.id])}
