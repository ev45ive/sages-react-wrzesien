import * as React from "react";
import { Album } from "src/model/Album";
import "./AlbumsGrid.css";

type IProps = {
  results: Album[];
};

export const AlbumsGrid: React.StatelessComponent<IProps> = ({ results }) => (
  <div className="albums-grid card-group">
    {results.map(album => (
      <div className="card" key={album.id} style={{}}>
        <img className="card-img-top" src={album.images[0].url} />
        <div className="card-body">
          <h5 className="card-title">{album.name}</h5>
        </div>
      </div>
    ))}
  </div>
);
