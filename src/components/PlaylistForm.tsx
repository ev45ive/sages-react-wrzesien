import * as React from "react";
import { Playlist } from "../model/Playlist";

export interface IProps {
  playlist: Playlist;
}

export interface IDispatchProps {
  onSave(playlist: Playlist): void;
  onCancel(): void;
}

export interface IState {
  playlistDraft?: Playlist;
  valid: boolean;
}

// type Partial<T> = { [k in keyof T]?: T[k] };

export class PlaylistForm extends React.Component<IProps & IDispatchProps, IState> {
  state: IState = {
    valid: false
  };

  handleFieldChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    const target = event.target;
    const name = target.name;
    const value = target.type == "checkbox" ? target.checked : target.value;

    this.setState(nextState => {
      const playlist = {
        ...nextState.playlistDraft!,
        [name]: value
      };
      return { playlistDraft: playlist };
    });
    this.validate();
  };

  static getDerivedStateFromProps(
    nextProps: IProps,
    nextState: IState
  ): Partial<IState> {
    return {
      playlistDraft: nextState.playlistDraft || nextProps.playlist
    };
  }

  save = () => {
    this.props.onSave(this.state.playlistDraft!);
  };

  validate() {
    this.setState(nextState => {
      let valid = false;

      if (nextState.playlistDraft && nextState.playlistDraft.name.length >= 3) {
        valid = true;
      }
      return {
        valid: valid
      };
    });
  }

  render() {
    const playlist = this.state.playlistDraft!;

    return this.props.playlist ? (
      <div>
        {this.state.valid == false && <p>Form Invalid</p>}

        <form>
          <div className="form-group">
            <label>Name:</label>
            <input
              type="text"
              className="form-control"
              onChange={this.handleFieldChange}
              value={playlist.name}
              name="name"
            />{" "}
            {50 - playlist.name.length}
          </div>

          <div className="form-group">
            <label>Favourite</label>
            <input
              type="checkbox"
              onChange={this.handleFieldChange}
              checked={playlist.favourite}
              name="favourite"
            />
          </div>

          <div className="form-group">
            <label>Color:</label>
            <input
              type="color"
              value={playlist.color}
              name="color"
              onChange={this.handleFieldChange}
            />
          </div>
          <input
            type="button"
            value="Cancel"
            className="btn btn-danger"
            onClick={this.props.onCancel}
          />
          <input
            type="button"
            value="Save"
            className="btn btn-success"
            onClick={this.save}
          />
        </form>
      </div>
    ) : <div/>;
  }
}
