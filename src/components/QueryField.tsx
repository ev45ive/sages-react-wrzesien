import * as React from "react";

export interface IProps {
  onSearch(query: string): void;
  query?: string;
  inputRef?: React.RefObject<HTMLInputElement>;
}

export class QueryField extends React.Component<IProps> {
  search = (event: React.ChangeEvent<HTMLInputElement>) => {
    this.props.onSearch(event.target.value);
  };

  render() {
    return (
      <div>
        <div className="form-group">
          <input
            type="text"
            className="form-control"
            onChange={this.search}
            value={this.props.query}
            ref={this.props.inputRef}
          />
        </div>
      </div>
    );
  }
}
