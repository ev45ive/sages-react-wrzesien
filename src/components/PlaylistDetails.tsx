import * as React from "react";
import { Playlist } from "../model/Playlist";

export interface IProps {
  playlist?: Playlist;
}

export class PlaylistDetails extends React.Component<IProps> {

  shouldComponentUpdate(nextProps:IProps){
    return this.props.playlist !== nextProps.playlist
  }


  render() {
    return this.props.playlist ? (
      <div>
        {/* dl>(dt+dd) */}

        <dl>
          <dt>Name:</dt>
          <dd> {this.props.playlist.name} </dd>

          <dt>Favourite:</dt>
          <dd> {this.props.playlist.favourite ? "Yes" : "No"} </dd>

          <dt>Color:</dt>
          <dd style={{
            backgroundColor: this.props.playlist.color
          }}> {this.props.playlist.color} </dd>
        </dl>
      </div>
    ) : <p>Please select playlist</p>
  }
}
