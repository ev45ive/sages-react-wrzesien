/**
 *
 */
export class Security {
  constructor(
    private url = "https://accounts.spotify.com/authorize",
    private client_id = "0eebaf9ee5ab40ce8801918367569cf0",
    private response_type = "token",
    private redirect_uri = "http://localhost:3000/"
  ) {}

  authorize() {
    const url = `${this.url}?client_id=${this.client_id}&response_type=${
      this.response_type
    }&redirect_uri=${this.redirect_uri}`;

    location.replace(url);
  }

  token = "";

  getToken() {
    this.token = JSON.parse(localStorage.getItem("token") || "null");

    if (!this.token && location.hash) {
      const match = location.hash.match(/access_token=([^&]*)/);
      this.token = (match && match[1]) || "";

      localStorage.setItem("token", JSON.stringify(this.token));
    }

    if (!this.token) {
      this.authorize();
    }

    return this.token;
  }
}
