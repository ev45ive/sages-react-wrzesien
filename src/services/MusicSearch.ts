import { Security } from "./Security";
import { AlbumsResponse } from "../model/Album";

export class MusicSearch {
  constructor(private security: Security) {}

  search(query = "batman") {
    const token = this.security.getToken();

    if (!query) {
      return Promise.resolve([]);
    }

    return fetch(`https://api.spotify.com/v1/search?type=album&q=${query}`, {
      headers: {
        Authorization: "Bearer " + token
      }
    })
      .then<AlbumsResponse>(resp => resp.json())
      .then(resp => resp.albums.items);
  }

  // search('batman').then(data => console.log(data))
}
