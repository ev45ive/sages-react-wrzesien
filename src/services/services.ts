import { Security } from "./Security";
import { MusicSearch } from "./MusicSearch";
import { loadResults, setQuery } from "../reducers/search";
import { Dispatch } from "redux";

const security = new Security();
security.getToken();
const musicSearch = new MusicSearch(security);

let debounce: NodeJS.Timer;

export const fetchSearchResults = (dispatch: Dispatch) => (query: string) => {
  dispatch(setQuery(query));

  clearTimeout(debounce);
  debounce = setTimeout(() => {
    musicSearch.search(query).then(results => {
      dispatch(loadResults(results));
    });
  }, 400);
};

// f = dispatch => query => dispatch(setQUery(query));
// ==
// function f(dispatch) {
//   return function g(query) {
//     dispatch(setQUery(query));
//   };
// }
