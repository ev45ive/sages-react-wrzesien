export interface Entity {
  id: string;
}

export interface Album extends Entity {
  name: string;
  artists: Artist[];
  images: AlbumImage[];
}

export interface Artist extends Entity {
  name: string;
}

export interface AlbumImage {
  url: string;
  height?: number;
  width?: number;
}

export interface PagingObject<T> {
  items: T[];
  total: number;
}

export interface AlbumsResponse {
  albums: PagingObject<Album>;
}
