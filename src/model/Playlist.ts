// abstract class Playlist{
//   id: number
// }

export interface Playlist {
  id: number;
  name: string;
  favourite: boolean;
  color: string;
  // tracks: Array<Track>;
  /**
   * Playlist Tracks
   */
  tracks?: Track[];
}

export interface Track {
  id: number;
  name: string;
}

// class X implements Playlist{}
// const X:Playlist = {}
