import * as React from "react";

import { PlaylistsView } from "./views/PlaylistsView";
import { MusicSearchView } from "./views/MusicSearchView";
import { Route, Redirect, Switch, NavLink } from "react-router-dom";

class App extends React.Component<{}> {
  public render() {
    return (
      <>
        <nav className="navbar navbar-expand navbar-dark bg-dark mb-3">
          <div className="container">
            <a className="navbar-brand" href="#">
              Music App
            </a>

            <div className="collapse navbar-collapse">
              <ul className="navbar-nav">
                <li className="nav-item">
                  <NavLink
                    className="nav-link"
                    to="/playlists"
                    activeClassName="active placki"
                  >
                    Playlists
                  </NavLink>
                </li>

                <li className="nav-item">
                  <NavLink className="nav-link" to="/search">
                    Music Search
                  </NavLink>
                </li>
              </ul>
            </div>
          </div>
        </nav>

        <div className="container">
          <div className="row">
            <div className="col">
              <Switch>
                <Route path="/playlists" component={PlaylistsView} />
                <Route path="/search" component={MusicSearchView} />
                <Redirect path="" exact={true} to="/playlists" />
                <Redirect path="**" to="/search" />
              </Switch>
            </div>
          </div>
        </div>
      </>
    );
  }
}

export default App;
