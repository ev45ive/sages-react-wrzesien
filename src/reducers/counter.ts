import { Reducer, Action, ActionCreator } from "redux";
export type counterState = number;

interface INCREMENT extends Action<"INCREMENT"> {
  payload: number;
}

interface DECREMENT extends Action<"DECREMENT"> {
  payload: number;
}

type Actions = INCREMENT | DECREMENT;

export const counter: Reducer<counterState, Actions> = (state = 0, action) => {
  switch (action.type) {
    case "INCREMENT":
      return state + action.payload;
    case "DECREMENT":
      return state - action.payload;
    default:
      return state;
  }
};

export const increment: ActionCreator<INCREMENT> = (payload = 1) => ({
  type: "INCREMENT",
  payload
});

export const decrement: ActionCreator<DECREMENT> = (payload = 1) => ({
  type: "DECREMENT",
  payload
});

export const actions = { increment, decrement };

window["actions"] = actions;
