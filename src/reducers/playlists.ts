import { Action, Reducer, ActionCreator } from "redux";
import { Playlist } from "src/model/Playlist";

export type PlaylistsState = {
  playlists: Playlist[];
  selectedId?: Playlist["id"];
};

const initialState: PlaylistsState = {
  playlists: [
    {
      id: 123,
      name: "Redux HIts!",
      color: "#ff0000",
      favourite: false
    },
    {
      id: 234,
      name: "The Best of React!",
      color: "#00ff00",
      favourite: false
    },
    {
      id: 345,
      name: "React TOP 20!",
      color: "#00ff00",
      favourite: true
    }
  ]
};

interface PLAYLIST_ADDED extends Action<"PLAYLIST_ADDED"> {
  payload: Playlist;
}

interface PLAYLIST_SELECTED extends Action<"PLAYLIST_SELECTED"> {
  payload: Playlist["id"];
}

interface PLAYLIST_UPDATED extends Action<"PLAYLIST_UPDATED"> {
  payload: Playlist;
}

type Actions = PLAYLIST_ADDED | PLAYLIST_SELECTED | PLAYLIST_UPDATED;

export const playlists: Reducer<PlaylistsState, Actions> = (
  state = initialState,
  action
) => {
  switch (action.type) {
    case "PLAYLIST_ADDED":
      return {
        ...state,
        playlists: [...state.playlists, action.payload]
      };
    case "PLAYLIST_UPDATED":
      return {
        ...state,
        playlists: state.playlists.map(
          p => (p.id == action.payload.id ? action.payload : p)
        )
      };
    case "PLAYLIST_SELECTED":
      return {
        ...state,
        selectedId:
          state.selectedId == action.payload ? undefined : action.payload
      };
    default:
      return state;
  }
};

export const addPlaylist: ActionCreator<PLAYLIST_ADDED> = (
  payload: Playlist
) => ({
  type: "PLAYLIST_ADDED",
  payload
});

export const selectPlaylist: ActionCreator<PLAYLIST_SELECTED> = (
  payload: Playlist["id"]
) => ({
  type: "PLAYLIST_SELECTED",
  payload
});

export const updatePlaylist: ActionCreator<PLAYLIST_UPDATED> = (
  payload: Playlist
) => ({
  type: "PLAYLIST_UPDATED",
  payload
});
