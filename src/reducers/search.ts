import { Reducer, Action, ActionCreator } from "redux";
import { Album } from "src/model/Album";

export interface SearchState {
  query: string;
  results: Album[];
}

const initialState: SearchState = {
  query: "",
  results: []
};

interface RESULTS_LOAD extends Action<"RESULTS_LOAD"> {
  payload: Album[];
}

interface SET_QUERY extends Action<"SET_QUERY"> {
  payload: string;
}

type Actions = RESULTS_LOAD | SET_QUERY;

export const search: Reducer<SearchState> = (
  state = initialState,
  action: Actions
) => {
  switch (action.type) {
    case "RESULTS_LOAD":
      return { ...state, results: action.payload };
    case "SET_QUERY":
      return { ...state, query: action.payload };
    default:
      return state;
  }
};

export const loadResults: ActionCreator<RESULTS_LOAD> = (payload: Album[]) => ({
  type: "RESULTS_LOAD",
  payload
});

export const setQuery: ActionCreator<SET_QUERY> = (payload: string) => ({
  type: "SET_QUERY",
  payload
});
