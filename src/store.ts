import { createStore, combineReducers } from "redux";
import { counter } from "src/reducers/counter";
import { counterState } from "./reducers/counter";
import { playlists, PlaylistsState } from "./reducers/playlists";
import { SearchState, search } from "./reducers/search";

export interface AppState {
  nested: {
    counter: counterState;
  };
  playlists: PlaylistsState;
  search: SearchState;
}
const reducer = combineReducers<AppState>({
  nested: combineReducers({ counter }),
  playlists,
  search
});

export const store = createStore(reducer);

// const reducer: Reducer<AppState> = (
//   state = {
//     counter: 0,
//     playlists: {
//       playlists: []
//     }
//   },
//   action: any
// ) => {
//   return {
//     counter: counter(state.counter, action),
//     playlists: playlists(state.playlists, action)
//   };
// };

window["store"] = store;
