import * as React from "react";
import * as ReactDOM from "react-dom";
import App from "./App";
import "./index.css";

import "bootstrap/dist/css/bootstrap.css";

// import { HashRouter as Router } from "react-router-dom";
import { BrowserRouter as Router } from "react-router-dom";

import registerServiceWorker from "./registerServiceWorker";
import { store } from "./store";
import { Provider } from "react-redux";

ReactDOM.render(
  <Provider store={store}>
    <Router>
      <App />
    </Router>
  </Provider>,
  document.getElementById("root") as HTMLElement
);
registerServiceWorker();

window["React"] = React;
window["ReactDOM"] = ReactDOM;
